//
// Created by Talysson Castro on 6/8/20.
//

#ifndef GILDED_ROSE_REFACTORING_KATA_CPP_QUALITYCHANGETYPEFACTORY_H
#define GILDED_ROSE_REFACTORING_KATA_CPP_QUALITYCHANGETYPEFACTORY_H


#include "Quality.h"
#include "QualityChangeType.h"

class QualityChangeTypeFactory {
public:
    static Quality* create(const QualityChangeType& qualityChangeType);
};


#endif //GILDED_ROSE_REFACTORING_KATA_CPP_QUALITYCHANGETYPEFACTORY_H
