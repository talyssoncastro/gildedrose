//
// Created by Talysson Castro on 6/8/20.
//

#include "QualityChangeTypeFactory.h"

#include <stdexcept>

#include "QualityNoChange.h"
#include "QualityStandard.h"
#include "QualityVariety.h"

Quality* QualityChangeTypeFactory::create(const QualityChangeType& qualityChangeType) {

    switch (qualityChangeType) {
        case QualityChangeType::NOCHANGE:
            return new QualityNoChange();
        case QualityChangeType::STANDARD:
            return new QualityStandard();
        case QualityChangeType::VARIETY:
            return new QualityVariety();
        default:
            throw std::invalid_argument( "Tipo não implementado" );
    }
}
