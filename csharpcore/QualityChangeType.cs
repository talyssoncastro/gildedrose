namespace csharpcore
{
    public enum QualityChangeType
    {
        NO_CHANGE,
        STANDARD,
        VARIETY,
        NONE
    }
}