﻿namespace csharpcore
{
    public class Item
    {
        public string Name { get; set; }
        public int SellIn { get; set; }
        public int Quality { get; set; }
        public int ChangeQuality { get; set; }
        public int ChangeSellIn { get; set; }
        public QualityChangeType QualityChangeType { get; set; }
        public void SetCurrentQuality(int Value) {
        if (Value > 50) {
            Value = 50;
        } else if (Value < 0) {
            Value = 0;
        }

        Quality = Value;
    }
    }
}
