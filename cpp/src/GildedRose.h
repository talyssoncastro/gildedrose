#include <string>
#include <vector>

#include "QualityChangeType.h"
#include "Item.h"

using namespace std;

class GildedRose
{
public:
    vector<Item> & items;
    GildedRose(vector<Item> & items);
    
    void updateQuality();
};

