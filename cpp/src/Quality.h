//
// Created by Talysson Castro on 6/8/20.
//

#ifndef GILDED_ROSE_REFACTORING_KATA_CPP_QUALITY_H
#define GILDED_ROSE_REFACTORING_KATA_CPP_QUALITY_H

#include "Item.h"

class Quality {
public:
    virtual void update(Item* item) { }
};


#endif //GILDED_ROSE_REFACTORING_KATA_CPP_QUALITY_H
