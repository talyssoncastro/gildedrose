#include "GildedRose.h"

#include "QualityChangeTypeFactory.h"

GildedRose::GildedRose(vector<Item> & items) : items(items)
{}

void GildedRose::updateQuality() {

    for (int i = 0; i < items.size(); i++) {

        Item* item = &items[i];

        Quality* quality = QualityChangeTypeFactory::create(item->qualityChangeType);

        quality->update(item);

        item->sellIn = item->sellIn + item->changeSellIn;

    }
}