//
// Created by Talysson Castro on 6/8/20.
//

#include "QualityVariety.h"

void QualityVariety::update(Item *item) {
    Quality::update(item);
    int tempQuality;
    if (item->sellIn <= 0) {
        tempQuality = 0;
    } else if (item->sellIn <= 5) {
        tempQuality = item->quality + 3;
    } else if (item->sellIn <= 10) {
        tempQuality = item->quality + 2;
    } else {
        tempQuality = item->quality + item->changeQuality;
    }

    item->setQuality(tempQuality);
}
