//
// Created by Talysson Castro on 6/8/20.
//

#include "QualityStandard.h"

void QualityStandard::update(Item *item) {
    Quality::update(item);
    int tempQuality = item->quality + (item->sellIn > 0 ? item->changeQuality : item->changeQuality * 2);
    item->setQuality(tempQuality);
}
