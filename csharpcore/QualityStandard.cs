namespace csharpcore
{
    public class QualityStandard: Quality
    {
        void Quality.Update(Item Item) {
            int TempQuality = Item.Quality + (Item.SellIn > 0 ? Item.ChangeQuality : Item.ChangeQuality * 2);
            Item.SetCurrentQuality(TempQuality);
        }
    }
}