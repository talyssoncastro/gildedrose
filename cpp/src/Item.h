//
// Created by Talysson Castro on 6/8/20.
//

#ifndef GILDED_ROSE_REFACTORING_KATA_CPP_ITEM_H
#define GILDED_ROSE_REFACTORING_KATA_CPP_ITEM_H

#include <string>

#include "QualityChangeType.h"

using namespace std;

class Item
{
public:
    string name;
    int sellIn;
    int quality;
    int changeQuality;
    int changeSellIn;
    QualityChangeType qualityChangeType;
    Item(string name, int sellIn, int quality, int changeQuality = 0, int changeSellIn = 0, QualityChangeType qualityChangeType = QualityChangeType::NONE) : name(name), sellIn(sellIn), quality(quality), changeQuality(changeQuality), changeSellIn(changeSellIn), qualityChangeType(qualityChangeType)
    {}
    void setQuality(int value) {
        if (value > 50) {
            value = 50;
        } else if (value < 0) {
            value = 0;
        }

        this->quality = value;
    }
};


#endif //GILDED_ROSE_REFACTORING_KATA_CPP_ITEM_H
