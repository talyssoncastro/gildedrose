namespace csharpcore
{
    public class QualityChangeTypeFactory
    {
        public static Quality Create(QualityChangeType QualityChangeType) {
            switch (QualityChangeType) {
                case QualityChangeType.NO_CHANGE:
                    return new QualityNoChange();
                case QualityChangeType.STANDARD:
                    return new QualityStandard();
                case QualityChangeType.VARIETY:
                    return new QualityVariety();
                default:
                    throw new System.InvalidOperationException("Invalid option");
            }
        }
    }
}