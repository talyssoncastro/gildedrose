//
// Created by Talysson Castro on 6/8/20.
//

#ifndef GILDED_ROSE_REFACTORING_KATA_CPP_QUALITYSTANDARD_H
#define GILDED_ROSE_REFACTORING_KATA_CPP_QUALITYSTANDARD_H


#include "Quality.h"

class QualityStandard: public Quality {
public:
    void update(Item* item);
};


#endif //GILDED_ROSE_REFACTORING_KATA_CPP_QUALITYSTANDARD_H
