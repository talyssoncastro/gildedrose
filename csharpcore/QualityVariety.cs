namespace csharpcore
{
    public class QualityVariety: Quality
    {
        void Quality.Update(Item Item) {
            int TempQuality;
            if (Item.SellIn <= 0) {
                TempQuality = 0;
            } else if (Item.SellIn <= 5) {
                TempQuality = Item.Quality + 3;
            } else if (Item.SellIn <= 10) {
                TempQuality = Item.Quality + 2;
            } else {
                TempQuality = Item.Quality + Item.ChangeQuality;
            }

            Item.SetCurrentQuality(TempQuality);
        }
    }
}