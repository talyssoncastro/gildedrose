namespace csharpcore
{
    public interface Quality
    {
        void Update(Item Item);
    }
}