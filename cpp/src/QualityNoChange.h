//
// Created by Talysson Castro on 6/8/20.
//

#ifndef GILDED_ROSE_REFACTORING_KATA_CPP_QUALITYNOCHANGE_H
#define GILDED_ROSE_REFACTORING_KATA_CPP_QUALITYNOCHANGE_H


#include "Quality.h"

class QualityNoChange: public Quality {
public:
    void update(Item* item);
};


#endif //GILDED_ROSE_REFACTORING_KATA_CPP_QUALITYNOCHANGE_H
